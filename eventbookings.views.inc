<?php 
/**
 * @file
 * Add in calendar views for bookable resources
 *
 */

function eventbookings_views_data() {
  $data['event_booking']['table'] = array(
    'group' => 'Event Bookings',
    'title' => 'Booking records for an event',
    'help' => 'All the booking records associated with an Event',
  );  
  $data['event_booking']['table']['join']['node'] = array(
    'left_table' => 'eventbookings_node_record',
    'left_field' => 'record_id',
    'field' => 'record_id',
    'table' => 'bookings_records',
  );
  $data['eventbookings_node_record']['table']['join']['node'] = array(
    'left_field' => 'nid',
    'field' => 'nid',
  );
  $data['event_booking']['record_id'] = array(
    'title' => t('ID of an event booking record'),
    // Information for displaying the nid
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'argument' => array(
       'title' => t('ID of an event booking record'),
       'handler' => 'views_handler_argument_numeric',
       'help' => t('With this argument, you will be able to fetch the booking records for an event'),
     ),
    // Information for accepting a nid as a filter
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    // Information for sorting on a nid.
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  ); 
  $data['event_booking']['resource_id'] = array(
    'title' => t('Event Resource'),
    'help' => t('The resource the record is referring.'),
    'relationship' => array(
      'base' => 'node',
      'field' => 'nid',
      'title' => t('Booked resources for an event'),
      'help' => t('Bring in all the booked resources for an event.'),
      'handler' => 'views_handler_relationship',
      'label' => t('Booked resources for an event'),
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'name field' => 'resource_id', // the field to display in the summary.
      'numeric' => TRUE,
    ),
  );
  $data['event_booking']['type'] = array(
    'title' => t('Booking Type'),
    'help' => t('The record type.'),
    'field' => array(
      'handler' => 'views_handler_field_bookings_record_types',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_bookings_record_types',
    ),
   'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'name field' => 'type', // the field to display in the summary.
      'numeric' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  return $data;
}
