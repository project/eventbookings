/**
 * Bind to availability checkbox and enable/disable status select
 */
$(document).ready(function() { 
  $('input.availability').change(eventbookings_set_booking_status).each(eventbookings_set_booking_status);
  $('input.all-day-event').change(eventbookings_enable_times).each(eventbookings_enable_times);
});

/**
 * Enable/disable status select
 */
function eventbookings_set_booking_status(){
  var checked = this.checked;
  $('.'+ this.getAttribute('content_type') + '_status').each(function() {
    this.disabled = (checked) ? 'disabled' : null;
  });
}

function eventbookings_enable_times(){
  var checked = this.checked;
  $('.'+ this.getAttribute('content_type') + '-time-select').each(function() {
    this.disabled = (checked) ? null : 'disabled';
  });
}