/**
 * When any value within the date field on an event changes, refresh the 
 * resource bookings table
 */
$(document).ready(function() { 
  var date_field = Drupal.settings.eventbookings.date_field;
    
  // Grab all input elements within the date fieldset
  var input_set = $('#' + date_field + ' :input[type!="checkbox"]');
  
  // mark initial state
  $.each(input_set, function(index, input) { 
    input.prev = input.value;
    input.dirty = false;
  });
  
  // bind to events
  input_set.change(eventbookings_refreshResources);
  input_set.blur(eventbookings_refreshResources);
  input_set.keypress(eventbookings_refreshResources);

  // always refresh for checkbox
  $('#' + date_field + ' :input[type="checkbox"]').click(eventbookings_refresh);
});

/***
 * Bind to the clear button
 */
Drupal.behaviors.eventbookings = function (context) {
  var clear_button = Drupal.settings.eventbookings.clear_button;
  var checkbox_type = Drupal.settings.eventbookings.checkbox_type;
  var eventbookings = Drupal.settings.eventbookings.eventbookings;
  
  // bind to clear button
  var button = $('#' + clear_button);
  button.click(eventbookings_clearSelection);
  
  //bind to inputs 
  var query = '#' + eventbookings + ' :' + checkbox_type;
  var input_set = $(query);
  input_set.click(eventbookings_set_status);
  eventbookings_set_status();
  $('#eventbookings-bookings-reload').mousedown(eventbookings_disable_submit);
}

/***
 * Delay posting to give changes time to propagagte
 */
var eventbookings_timer = null;
function eventbookings_refreshResources(e) {
  if ( eventbookings_timer != null ) {
    window.clearTimeout( eventbookings_timer );
    eventbookings_timer = null;
  }
  eventbookings_timer = window.setTimeout( function() { 
    _eventbookings_refreshResources(e);
  }, 500 );
}

/**
 * Refresh the booking resources when a value in the date widget changes
 */
var eventbookings_queued = false;
function _eventbookings_refreshResources(e) {
  if ( eventbookings_timer != null ) {
    window.clearTimeout( eventbookings_timer );
    eventbookings_timer = null;
  }
  var changed = ( typeof e.target.prev == "undefined" || e.target.prev != e.target.value );
  var button = $('#eventbookings-bookings-reload');

  // If there is a button and the value has changed....
  if ( button !== null && changed ) {
    
    // If a request is in progress, queue this request...
    if( button.hasClass('progress-disabled') ) {
      if ( !eventbookings_queued ) {
        window.setTimeout(function(){
          eventbookings_queued = false;
          e.target.dirty = false;
          eventbookings_refreshResources(e);
        }, 50);
        e.target.dirty = true;
        eventbookings_queued = true;
      }
      else {
        // Since we're swallowing this event, save state unless it's marked as dirty
        if ( !e.target.dirty ) {
          e.target.prev = e.target.value;
        }
      }
    }
    else {
      // Post the form and mark new state
      eventbookings_refresh();
      e.target.prev = e.target.value;
    }
  }
  else if ( button == null ){
    alert('Unable to locate resource refresh button');
  }
}

/***
 * Invoke the refresh button
 */
function eventbookings_refresh() {
  $('#eventbookings-bookings-reload').trigger('mousedown');
}

/***
 * Clear the selections and removed saved state from the bookings table
 */
function eventbookings_clearSelection(e) {
  var eventbookings = Drupal.settings.eventbookings.eventbookings;
  var saved_selection = Drupal.settings.eventbookings.saved_selection
  var input_set = $('#' + eventbookings + ' :input:checked');
  input_set.removeAttr('checked');
  $('#' + saved_selection).remove();
  eventbookings_set_status();
  return false;
}

/***
 * Set the visibility of the status field
 */
function eventbookings_set_status() {
  var checkbox_type = Drupal.settings.eventbookings.checkbox_type;
  var eventbookings = Drupal.settings.eventbookings.eventbookings;
  
  //bind to inputs 
  var query = '#' + eventbookings + ' :' + checkbox_type;
  var input_set = $(query);
  input_set.each(function(i,input) {
    eventbookings_toggle_ele(input, 'status');
  });
}

/***
 * Toggle disabled status for an element
 */
function eventbookings_toggle_ele(input, repl) {
  var selector = '#' + input.id.replace('select', repl);
  var ele = $(selector);
  if (ele.size() > 0) {
    if (input.checked && !input.disabled) {
      ele.removeAttr('disabled');
    }
    else {
      ele.attr('disabled','disabled');
    }
  }
}

/***
 * Disable the submit buttons when the eventbookings form is posted - this prevents
 * errors caused by ajax interruptions
 */
function eventbookings_disable_submit() {
  $('.form-submit').attr('disabled','disabled').ajaxComplete(eventbookings_enable_submit);
}

/***
 * Enable submit buttons
 */
function eventbookings_enable_submit() {
  $('.form-submit').removeAttr('disabled');
}